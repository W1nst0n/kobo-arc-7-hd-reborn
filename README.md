# Kobo Arc 7 HD

## Requirements

- Windows (I tried really hard to find a Linux-only solution but I didn't manage for the *root* part)
- [Google USB Driver for Windows](https://dl.google.com/android/repository/usb_driver_r12-windows.zip)
- [Plateforme Tools for Windows](https://dl.google.com/android/repository/platform-tools-latest-windows.zip)
- USB debugging
- Allow Installation from Unknown Sources (Security section)

If you can't pass the SetupWizard screen (because of an error saying there is something wrong with your wifi), here's the hack:

- When The keyboard pops for the wifi password, press on the keyboard settings button (the gear at the bottom left)
- Select `Google Keyboard Settings`
- Select `Personnal dictionnary`
- You're now in the Settings apps. You can can press on the top left return button to go back to the settings main menu.
- Go to `About Kobo Arc 7HD`
- Tap 7 times on "Build  number". This will activate the `Developer options`
- In `Developer options` section, activate `USB debugging`.
- In `Security` section, check `Unknown source`.


Fastest way to install Google USB drivers on Windows is to right-clic on the `android_winusb.inf` --> `Install`.

On Linux you only need to install Android plateform-tools (Google drivers are unecessary).

``` bash
apt-get install android-sdk-platform-tools  # Debian
sudo pacman -S android-tools                # Archlinux
```

## Firmware

Unfortunately. There is no custom ROM available for this tablet. Be sur to run the last Kobo firmware ([JDQ39.339](https://kbdownload1-a.akamaihd.net/apollohd/tls-update-ota339.zip)).
To flash it manually you need to rename the file to `update.zip` and push it to `/sdcard/` :

``` bash
adb push update.zip /sdcard/
```

## Root 

The last version of Cydia Impactor does not work with the device. You need [v0.9.14](cache.saurik.com/impactor/win/Impactor_0.9.14.zip). No Linux build available :(

If you correctly installed Google USB drivers, Cydia Impactor should have detected the Kobo Arc 7 HD. You just have to click on `Start` with the dropdown menu set to `drop SuperSU su to /system/xbin/su`.

I'd have preferred to use [Magisk](https://github.com/topjohnwu/Magisk) over SuperSU but it doesn't work unfortunately and I don't have the time to figure out why. Let me know if you have success with it! :)

Now you can open a root adb shell : 

``` bash
adb shell su
``` 

## Custom Recovery

[@dazza9075](https://forum.xda-developers.com/member.php?s=aebd7dd09587ab4d25680f792e97c255&u=547180) created a custom recovery for the Kobo Arc 7 HD tablet. \o/

Download the [recovery.img](https://forum.xda-developers.com/attachment.php?s=aebd7dd09587ab4d25680f792e97c255&attachmentid=2536994&d=1390597215) ([Git repo](https://github.com/dazza9075/Kobo-Arc7HD-Recovery))

``` bash
adb push recovery.img /sdcard/
adb shell su
dd if=/sdcard/recovery.img of=/dev/block/platform/sdhci-tegra.3/by-name/SOS
exit
adb reboot recovery
```
Make sure you allow recovery to make changes when prompted or it will automatically revert to stock recovery

## Heavy debloat 

There is a lot of bloat on this device. I removed a **LOT** of things. Have a look at it before using it. Keep in mind that the device is stuck on Android Jelly Bean (4.2) so there is not much you can install. The less you have the better it is ! :)

Push the *debloat_kobo.sh* on `/sdcard` et execute it from an adb root shell ! 

NB : If you deleted something by mistake you can restore from the [official firmware](https://kbdownload1-a.akamaihd.net/apollohd/tls-update-ota339.zip).

## Apps installation

A lot of apps I'd have liked to install no longer support Android 4.2. I found lightweight FOSS alternatives to some of them on the [F-droid](https://f-droid.org/F-Droid.apk) app store.

``` bash
adb install F-droid.apk
```

- Ebook reader : [Book Reader](https://f-droid.org/fr/packages/com.github.axet.bookreader/)
- File Manager : [File Manager](https://f-droid.org/fr/packages/com.github.axet.filemanager/) 
- Android Launcher : [Kiss Launcher](https://f-droid.org/en/packages/fr.neamar.kiss/) is **way faster** than Kobo Launcher
- Keyboard : [AOSP Keyboard](https://www.apkmirror.com/apk/lineageos/android-keyboard-aosp-2/android-keyboard-aosp-2-7-1-2-release/)is way faster than the Google Keyboard and doesn't ping home all the time.


For those who want some (FOSS) games : 
- Chess Game : [DroidFish](https://f-droid.org/fr/packages/org.petero.droidfish)
- Roguelike : [Shattered Pixel Dungeon](https://f-droid.org/fr/packages/com.shatteredpixel.shatteredpixeldungeon/)
- Turn-based strategy : [Tanks of Freedom](https://f-droid.org/fr/packages/in.p1x.tanks_of_freedom)


Don't forget to remove then Kobo Launcher :

``` bash
adb shell su
mount -o rw,remount -t yaffs2 /dev/block/mtdblock3 /system
rm -rf /system/app/Tapestries.*
```

