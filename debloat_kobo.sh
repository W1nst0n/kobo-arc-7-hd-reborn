#!/system/bin/sh

# Kobo Arc 7 HD

mount -o rw,remount -t yaffs2 /dev/block/mtdblock3 /system


# .odex files are also in /system/app

# DO NOT REMOVE
# /system/framework/framework-res.*                # android
# /system/app/Bluetooth.*                          # com.android.bluetooth
# /system/app/Calculator.*                         # com.android.calculator2
# /system/app/CertInstaller.*                      # com.android.certinstaller
# /system/app/DefaultContainerService.*            # com.android.defcontainer
# /system/app/Exchange2.*                          # com.android.exchange
# /system/app/Gallery2.*                           # com.android.gallery3d
# /system/app/PackageInstaller.*                   # com.android.packageinstaller
# /system/app/Phone.*                              # com.android.phone
# /system/app/ApplicationsProvider.*               # com.android.providers.applications
# /system/app/DownloadProvider.*                   # com.android.providers.downloads
# /system/app/DownloadProviderUi.*                 # com.android.providers.downloads.ui
# /system/app/MediaProvider.*                      # com.android.providers.media
# /system/app/SettingsProvider.*                   # com.android.providers.settings
# /system/app/Settings.*                           # com.android.settings
# /system/app/SystemUI.*                           # com.android.systemui
# /system/app/VpnDialogs.*                         # com.android.vpndialogs
# /system/app/FusedLocation.*                      # com.android.location.fused (BOOTLOOP)

# REMOVE AFTER INSTALLING A BETTER LAUNCHER
# /system/app/Tapestries.*                         # com.kobo.launcher

# EVEN WITH ANOTHER KEYBOARD YOU CAN'T DELETE THIS WITHOUT LOSING KEYBOARD ACCESS :(   
# /system/app/LatinImeGoogle.*                     # com.google.android.inputmethod.latin

rm -rf /system/app/DarkKnight.*                    # com.kobo.statusbar
rm -rf /system/app/SystemUtil.*                    # com.kobo.systemutil
rm -rf /system/app/app/Browser.*                   # com.android.browser
rm -rf /system/app/BackupRestoreConfirmation.*     # com.android.backupconfirm
rm -rf /system/app/BasicDreams.*                   # com.android.dreams.basic
rm -rf /system/app/Books.*                         # com.google.android.apps.books
rm -rf /system/app/Browser.*                       # com.android.browser
rm -rf /system/app/CalendarGoogle.*                # com.google.android.calendar
rm -rf /system/app/CalendarProvider.*              # com.android.providers.calendar
rm -rf /system/app/ChromeBookmarksSyncAdapter.*    # com.google.android.syncadapters.bookmarks
rm -rf /system/app/ChromeWithBrowser.*             # com.android.chrome
rm -rf /system/app/ConfigUpdater.*                 # com.google.android.configupdater
rm -rf /system/app/Contacts.*                      # com.android.contacts
rm -rf /system/app/ContactsProvider.*              # com.android.providers.contacts
rm -rf /system/app/CrowdCare.*                     # com.crowdcare.agent.k
rm -rf /system/app/DeskClock.*                     # com.android.deskclock
rm -rf /system/app/DrmProvider.*                   # com.android.providers.drm
rm -rf /system/app/Email.*                         # com.android.email
rm -rf /system/app/FaceLock.*                      # com.android.facelock
rm -rf /system/app/feedly.*                        # com.devhd.feedly
rm -rf /system/app/Galaxy4.*                       # com.android.galaxy4
rm -rf /system/app/Gmail2.*                        # com.google.android.gm
rm -rf /system/app/GMS_Maps.*                      # com.google.android.apps.maps
rm -rf /system/app/GmsCore.*                       # com.google.android.gms
rm -rf /system/app/GoogleBackupTransport.*         # com.google.android.backup
rm -rf /system/app/GoogleContactsSyncAdapter.*     # com.google.android.syncadapters.contacts
rm -rf /system/app/GoogleFeedback.*                # com.google.android.feedback
rm -rf /system/app/GoogleLoginService.*            # com.google.android.gsf.login
rm -rf /system/app/GooglePartnerSetup.*            # com.google.android.partnersetup
rm -rf /system/app/GoogleServicesFramework.*       # com.google.android.gsf
rm -rf /system/app/GoogleTTS.*                     # com.google.android.tts
rm -rf /system/app/Hangouts.*                      # com.google.android.talk
rm -rf /system/app/HoloSpiralWallpaper.*           # com.android.wallpaper.holospiral
rm -rf /system/app/HTMLViewer.*                    # com.android.htmlviewer
rm -rf /system/app/InputDevices.*                  # com.android.inputdevices
rm -rf /system/app/KeyChain.*                      # com.android.keychain
rm -rf /system/app/KoboJIT.* 
rm -rf /system/app/KoboReader.* 
rm -rf /system/app/KoboSetupWizard.* 
rm -rf /system/app/KoboStubs.* 
rm -rf /system/app/LiveWallpapers.*                # com.android.wallpaper
rm -rf /system/app/LiveWallpapersPicker.*          # com.android.wallpaper.livepicker
rm -rf /system/app/Magazines.*                     # com.google.android.apps.magazines
rm -rf /system/app/MagicSmokeWallpapers.*          # com.android.magicsmoke
rm -rf /system/app/MediaUploader.*                 # com.google.android.apps.uploader
rm -rf /system/app/Music2.*                        # com.google.android.music
rm -rf /system/app/MusicFX.*                       # com.android.musicfx
rm -rf /system/app/NetworkLocation.*               # com.google.android.location
rm -rf /system/app/NoiseField.*                    # com.android.noisefield
rm -rf /system/app/OneTimeInitializer.*            # com.google.android.onetimeinitializer
rm -rf /system/app/OpenWnn.*                       # jp.co.omronsoft.openwnn
rm -rf /system/app/OTAUpdater.*                    # com.kobo.otaupdater
rm -rf /system/app/PartnerBookmarksProvider.*      # com.android.providers.partnerbookmarks
rm -rf /system/app/PhaseBeam.*                     # com.android.phasebeam
rm -rf /system/app/Phonesky.*                      # com.android.vending
rm -rf /system/app/PlayGames.*                     # com.google.android.play.games
rm -rf /system/app/PlusOne.*                       # com.google.android.apps.plus
rm -rf /system/app/PocketForKobo.*                 # com.ideashower.readitlater.pro
rm -rf /system/app/Provision.*                     # com.android.provision
rm -rf /system/app/SharedStorageBackup.*           # com.android.sharedstoragebackup
rm -rf /system/app/SoundRecorder.*                 # com.android.soundrecorder
rm -rf /system/app/Street.*                        # com.google.android.street
rm -rf /system/app/System.*                        # com.kobo.et
rm -rf /system/app/TagGoogle.*                     # com.google.android.tag
rm -rf /system/app/talkback.*                      # com.google.android.marvin.talkback
rm -rf /system/app/TelephonyProvider.*             # com.android.providers.telephony
rm -rf /system/app/UserDictionaryProvider.*        # com.android.providers.userdictionary
rm -rf /system/app/Velvet.*                        # com.google.android.googlequicksearchbox
rm -rf /system/app/VideoEditorGoogle.*             # com.google.android.videoeditor
rm -rf /system/app/Videos.*                        # com.google.android.videos
rm -rf /system/app/VisualizationWallpapers.*       # com.android.musicvis
rm -rf /system/app/VoiceSearchStub.*               # com.google.android.voicesearch
rm -rf /system/app/YouTube.*                       # com.google.android.youtube

rm -rf /system/tts/
rm -rf /system/framework/com.google.widevine.software.drm.*
rm -rf /system/framework/com.google.android.maps.*
rm -rf /system/framework/com.android.location.provider.*
rm -rf /system/lib/libvideochat* /system/lib/libvideoeditor*

rm -rf /data/drm
rm -rf /data/dalvik-cache/*

echo "All done ! :)"